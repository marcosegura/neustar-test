<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class BasicTest extends TestCase
{

    /**
     *
     */
    public function testDnsPage()
    {
        $response = $this->get('/dns');
        $response->assertStatus(200);
    }

    public function testEnterListForm()
    {
        $response = $this->json('POST', '/dns/save', ['lists' => 'google.com,neustar.com']);
        $response->assertStatus(302);
    }

    public function testGetLists()
    {
        $response = $this->json('POST', '/dns/get');
        $test = array('dns' => 'google.com');
        $response
            ->assertStatus(200)
            ->assertJsonFragment($test);
    }
}
