<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DnsController@index');

Route::get('/dns', 'DnsController@index');

Route::post('/dns/save', 'DnsController@save');

Route::post('/dns/get', 'DnsController@getAllList');