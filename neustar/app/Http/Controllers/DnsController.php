<?php
/**
 * Created by PhpStorm.
 * User: msegura
 * Date: 2019-03-23
 * Time: 15:58
 */

namespace App\Http\Controllers;

use App\Http\Requests\CustomRequest;
use Illuminate\Support\Facades\DB;

class DnsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dns/index');
    }

    /**
     * @param CustomRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function save(CustomRequest $request)
    {
        //Validate if the hidden input comes with the list
        $validated = $request->validated();
        $param = '';
        if ($validated)
            $param = $request->input('lists');

        //Explode the list by ','
        $lists = explode(',', $param);
        $errorList = array();
        foreach ($lists as $list) {
            //Function to get the IP from the DNS list
            $ip = gethostbyname($list);
            if (filter_var($ip, FILTER_VALIDATE_IP)) {
                //Check if the DNS already exist
                $element = DB::table('dns')->where('ip', $ip)->first();
                if (empty($element->dns))
                    //Doing the insert in case of no
                    DB::table('dns')->insert(['dns' => $list, 'ip' => $ip]);
            } else {
                $errorList[] = $list;
            }
        }

        //If there is any DNS not valid, show warning message
        if (!empty($errorList)) {
            return redirect()->back()->withInput()->withErrors(['lists'=>'Warning: The next DNS records are not valid: ' . implode(", ", $errorList)]);
        }

        return redirect('/dns');
    }

    /**
     *Function that will return all the DNS from table, for Ajax calls
     */
    public function getAllList()
    {

        $lists = DB::table('dns')->get();
        return response()->json($lists);
    }
}
