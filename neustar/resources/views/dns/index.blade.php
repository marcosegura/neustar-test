@extends('layouts.app')

@section('title', 'Manage DNS')

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="container wrapper-dns">
        <h2 class="title">Neustar Test</h2>
        <form action="/dns/save" method="post">
            {{ csrf_field() }}
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <input type="hidden" id="values" name="lists" value=""/>
            <p>Please enter the desire DNS record on the next field, then, do click on the "add DNS Record" button to add the DNS to the temporal list, don't repeat the value if already
                exist on the list</p>
            <div class="form-group">
                <label for="title">DNS record</label>
                <input type="text" class="form-control" id="dnsName">
                <span class="error"></span>
            </div>
            <button type="button" class="btn btn-secondary margin-bottom" onclick="addToList();">Add DNS Record to the List</button>
            <div class="form-group">
                <h5>List of DNS Records</h5>
                <div class="lists">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

        <div class="container wrapper-results">
            <h2>DNS Record List</h2>
            <div class="table">
                <table id="dns-table" class="display dns-table" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>CNAME</th>
                        <th>IP</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <button type="button" class="btn btn-primary" onclick="doingAjax();">Refresh</button>
        </div>
        <script type="text/javascript">

            let lists = [];
            let table = '';

            /**
             *
             * Function that will validate if the DNS exist in order to put it on the array or not
             */
            function addToList() {
                let dns = jQuery('#dnsName');
                let data = dns.val().toLowerCase();
                let idx = $.inArray(data, lists);

                if (isDnsValid(data)) {
                    if (idx === -1) {
                        lists.push(data);
                        jQuery('.lists').append('<span class="rows">' + data + '</span>');
                        jQuery('#values').val(lists.join(','));
                        dns.val('');
                    } else {
                        showError('DNS record already entered!');
                    }
                } else {
                    showError('Please enter a correct DNS record');
                }
            }

            /**
             *
             *@param msg =  message with the error
             * Function in charge of print message errors
             */
            function showError(msg) {
                let error = jQuery('.error');
                error.html(msg);
                error.show(0).delay(3000).hide(0);
            }

            /**
             *
             * @param dns = DNS that comes from addToList function
             * @returns {boolean} Validate if the DNS has a correct format and structure
             */
            function isDnsValid(dns) {
                return /^(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(dns);
            }

            /**
             * Function in charge of do the Ajax call to get all the lists
             */
            function doingAjax(){
                jQuery.ajax({
                    url: "{{url('/')}}/dns/get",
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},
                    type: 'POST',
                }).success(function (data) {
                    table.clear();
                    table.rows.add(data);
                    table.draw();
                });
            }

            /**
             * Jquery function to initialize datatable
             */
            jQuery(document).ready(function () {
                table = jQuery('#dns-table').DataTable(
                    {
                        "responsive": true,
                        columns: [
                            { data: 'dns' },
                            { data: 'ip' }
                        ]
                    }
                );
                //First entry
                doingAjax();
            });
        </script>
    </div>
    <footer>
        <p>Candidate: Marco Segura</p>
        <p>Email: marco.segura.delgado@gmail.com</p>
    </footer>
@endsection
