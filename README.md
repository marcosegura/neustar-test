## Neustar Test project Documentation

**Introduction**

Laravel project, latest version 5.8, using sqlite as database env with just one table called "DNS", basically the project allow us to save a list of
dns on a table, and show then by Ajax, the program has a logic or verification of not adding same DNS twice, and DNS structure verification.
 ---
**Steps for site setup/configuration**

1. Do git clone https://marcosegura@bitbucket.org/marcosegura/neustar-test.git on your desire location
2. Install composer in case you don't have on your environment, then, execute 'composer install' at the root of the laravel project, means inside of the neustar folder.
2. Verify that you have apache/nginx installed on your environment, if not, please proceed to install it
3. Configure the web service tool you already installed, having a virtualhost configured, example
```
<VirtualHost *:80>
    ServerAdmin webmaster@localhost.com
    DocumentRoot "/project_root/neustar-test/neustar/public/"
    ServerName loc.neustar.com

   <Directory />
         Options FollowSymLinks
         AllowOverride All
   </Directory>
   <Directory /project_root/neustar-test/neustar/public/>
         Options Indexes FollowSymLinks MultiViews
         AllowOverride All
         Order allow,deny
         allow from all
   </Directory>
</VirtualHost>
```
4. For development cases, edit the file /etc/hosts and enter the ServerName you added on the virtualhost step
4. Verify if you have PHP installed on your environment, if not, please proceed to install the latest version
5. Verify if you have sqlite installed, if not, proceed with the installation
6. Edit .env file locate it on the root of the laravel project, and specify the correct path of the database, on the field "DB_DATABASE", the file is on the root of the project
---
**How to use**

1. Access the url you specified on the virtualhost process, for testing purposes, we will use 'loc.neustar.com'.
2. You can access the root of the site, or loc.neustar.com/dns as well
3. Enter the DNS desire, add it to the temporal list, and then click submit, to add the list to the table.
4. You can visualize all the records on the second section of the page, the records will be getting automatically when you refresh the page (ajax call), or well you can do click over refresh button
 ---
 **Unit Testing**
 
 1. In case you want to test the program, you can edit the rules that are right now on the file 'neustar/tests/Feature/BasicTest.php'
 2. To execute the test, you only have to execute on the root of the laravel project, the next command line "vendor/bin/phpunit"
 